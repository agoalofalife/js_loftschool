function splice(array,start,deleteCount,newElement) {
    let removed          = [],
        cache            = [],
        startCashe       = start,
        deleteCountCashe = deleteCount,
        end              =  start + deleteCount;

    switch (arguments.length) {
        case 0 :
            return removed;
        case 1 :
            return removed;
        case 2 :
            if (start > array.length) {
                return removed;
            }
            if (start < -array.length || start == undefined) {
                for (let i = 0; i < array.length;i++) {
                    removed[removed.length] = array[i];
                }
                array.length = 0;
                return removed;
            }
            if (start < array.length && start < 0) {
                for (let i = start+array.length; i < array.length;i++) {
                    removed[removed.length] = array[i];
                }
                for (let i = 0 ;i < start + array.length;i++) {
                    cache[cache.length] =  array[i];
                }
                sourceUpdateArray(cache,array);
                return removed;
            }
            break;
        case 3 :
            if ( start > array.length || start < 0 && deleteCount < 0 || start < 0 && deleteCount <= 0 || start >= 0 && deleteCount < 0) {
                return removed;
            }
            if ( start >= 0 && deleteCount >= 0) {
                while ( deleteCountCashe != 0  ) {
                    if (array[startCashe] === undefined) {
                        deleteCountCashe--;
                        continue;
                    }
                    removed[removed.length] = array[startCashe];
                    deleteCountCashe--;
                    startCashe++;
                }
                for (let i = 0 ;i < start;i++) {
                    cache[cache.length] =  array[i];
                }
                for (let i = end ;i < array.length;i++) {
                    cache[cache.length] =  array[i];
                }
                sourceUpdateArray(cache,array);
                return removed;
            }
            
            if ( start < 0 && deleteCount >= 0 ) {
                if (start < -array.length) {
                    start = -array.length;
                }
                let position =  start+array.length;
                for (let i = 0 ; i < deleteCount;i++) {
                    if (array[position] === undefined) {
                        position++;
                        continue;
                    }
                    removed[removed.length] = array[position];
                    position++;
                }
                 position =  start+array.length;

                //Записываем в буфер элементы массива до старта
                for (let i = 0 ;i < position;i++) {
                    cache[cache.length] =  array[i];
                }
                sourceUpdateArray(cache,array);
                 return removed;
            }
            break;
        default :

            if ( start > array.length || start < 0 && deleteCount < 0 || start < 0 && deleteCount <= 0 || start >= 0 && deleteCount < 0) {
                for (let i = 3 ;i < arguments.length;i++) {
                    cache[cache.length] =  arguments[i];
                }
                    for (let i = 0 ;i < array.length;i++) {
                        cache[cache.length] =  array[i];
                    }
                sourceUpdateArray(cache,array);
                return removed;
            }
            if ( start >= 0 && deleteCount >= 0) {
                while ( deleteCountCashe != 0  ) {
                    if (array[startCashe] === undefined) {
                        deleteCountCashe--;
                        continue;
                    }
                    removed[removed.length] = array[startCashe];
                    deleteCountCashe--;
                    startCashe++;
                }
                for (let i = 0 ;i < start;i++) {
                    cache[cache.length] =  array[i];
                }
                for (let i = 3 ;i < arguments.length;i++) {
                    cache[cache.length] =  arguments[i];
                }
                for (let i = end ;i < array.length;i++) {
                    cache[cache.length] =  array[i];
                }

                sourceUpdateArray(cache,array);
                return removed;
            }

            if ( start < 0 && deleteCount >= 0 ) {
                if (start < -array.length) {
                    start = -array.length;
                }
                let position =  start+array.length;
                for (let i = 0 ; i < deleteCount;i++) {
                    if (array[position] === undefined) {
                        position++;
                        continue;
                    }
                    removed[removed.length] = array[position];
                    position++;
                }
                position =  start+array.length;
                for (let i = 3 ;i < arguments.length;i++) {
                    cache[cache.length] =  arguments[i];
                }
                //Записываем в буфер элементы массива до старта
                for (let i = 0 ;i < position;i++) {
                    cache[cache.length] =  array[i];
                }
                sourceUpdateArray(cache,array);
                return removed;
            }
            break;
    }
    // update array
    function sourceUpdateArray(cache,array) {
        array.length = 0;
        for ( let i = 0; i < cache.length;i++) {
            array[array.length] = cache[i];
        }
    }
}

module.exports = {
    splice : splice
};