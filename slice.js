/** Custom function slice array **/
function slice(arr,begin,end) {
    let copyArray = [];
    if ( begin == undefined ) {
        copyArr(arr,copyArray);
    }
    if ( typeof begin != 'number') {
        if (isNaN(+begin) ) {
          return   copyArr(arr,copyArray);
        } else {
            begin = parseInt(begin);
        }
    }

    if(begin > arr.length || begin === end ) {
        return copyArray;
    }
    if ( arguments.length === 3 && begin < 0 && end < 0) {
        if ( begin > end ) {
            return copyArray;
        }
    }
    if ( arguments.length === 2 && begin < 0) {
        return arr.splice(arr.length + begin);
        }
    if( arguments.length === 2 && begin > 0) {
        return arr.splice(begin);
    }
    if ( arguments.length === 3 && begin > 0 && end > 0) {
        if ( begin > end ) {
            return copyArray;
        }
        for (begin;begin < end && begin < arr.length;begin++) {
            copyArray.push(arr[begin]);
        }
        return copyArray;
    }
     if ( arguments.length === 3 && begin < 0 && end >= 0 ) {
         return copyArray;
     }
    if ( arguments.length === 3 && begin < 0 && end < 0 ) {
        let start  = arr.length+begin;
        let finish = arr.length+end;
        
        
        for (start;start < finish;start++) {

            copyArray.push(arr[start]);
        }
        return copyArray;
    }
    if (arguments.length === 3 && begin >= 0 && end < 0) {

        for (begin;begin < arr.length+end;begin++) {

            copyArray.push(arr[begin]);
        }
        return copyArray;
    }
   function copyArr(arr,copyArray) {
       for ( let i  = 0;i < arr.length;i++ ) {
           copyArray[copyArray.length] = arr[i];
       }
       return copyArray;
   }
}

module.exports = {
    slice : slice
};