/** Custom function map array **/

function map(array,callback,context) {

    let newArrInFilter = [];
    for (let i = 0;i < array.length;i++) {
        if(i in array) {
            newArrInFilter.push(callback.call(context, array[i], i));
        }
    }
    return newArrInFilter;
}

module.exports = {
    map  : map
};
