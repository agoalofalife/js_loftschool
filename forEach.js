/**  Custom function forEach **/

function forEach(array,callback,context) {
    for (let i = 0;i < array.length;i++) {
        if(i in array) {
            callback.call(context,array[i],i);
        }

    }
}
module.exports = {
    forEach  : forEach
};
