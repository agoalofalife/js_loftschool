/** Custom function filter array **/
function filter(array,callback,context) {
    let newArrInFilter = [];
    for (let i = 0;i < array.length;i++) {
        if (callback.call( context,array[i],i) ) {
            newArrInFilter.push(array[i]);
        }
    }
    return newArrInFilter;
}


module.exports = {
    filter  : filter
};
