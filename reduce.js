/** Custom function reduce array **/
function reduce(arr, callback, initialValue) {
    let result;
    initialValue === undefined ?   result = [] : result = initialValue;
    for (let i  = 0; i < arr.length; i++) {
        result  = callback.call(null, result, arr[i], i, arr);
    }
    return result;
}

module.exports = {
    reduce  : reduce
};
